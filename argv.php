<?php

$argv = [
    'items' => [
        '517DC415F4C099269CD938EBD6C1F369E783E70B763182A777A97A4E35DC3663' => [
            'controlled' => [
                'name' => 'Kristian Vinkenes',
                'email' => 'kristian@nettbureau.no',
                'phone' => '12345678',
                'address' => 'Colletts gate 10A',
                'zip' => '0169'
            ],
            'raw' => [
                'system' => [
                    'sys_ip' => '8.8.8.8',
                    'sys_url' => 'https://example.com'
                ],
                'date' => '2019-03-27 10:02:18'
            ]
        ]
    ],
    'log' => [],
    'results' => [
        '517DC415F4C099269CD938EBD6C1F369E783E70B763182A777A97A4E35DC3663' => TRUE
    ],
    'messages' => []
];
